#include "PacWoman.h"

PacWoman::PacWoman(sf::Texture& texture)
	:m_visual(texture)
	, m_isDead(false)
{
	setOrigin(20, 20);
}



PacWoman::~PacWoman()
{
}

/*Se encarga de poner 
la varialbe de muerte activada
*/
void PacWoman::Die()
{
	m_isDead = true;
}

/*No se que quiere decir esta funcion*/
bool PacWoman::isDead() const
{
	return m_isDead;
}

void PacWoman::draw(sf::RenderTarget& target, sf::RenderStates states)const
{
	states.transform *= getTransform();
	if (!m_isDead)
	{
		target.draw(m_visual, states);
	}

}
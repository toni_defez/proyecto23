#ifndef  PACWOMAN_BONUS_HPP
#define PACWOMAN_BONUS_HPP
#include <SFML\Graphics.hpp>
/*
Esta clase se encuentra dedicada a la frutas
y demas elementos que otorgan una serie de puntos
extras

*/

/*
De esta manera podemos hacer que haya varias 
herencias a la vez o mejor dicho podemos acceder
a la parte publica de varias clases
*/
class Bonus :public sf::Drawable,public sf::Transformable
{
public:
	Bonus(sf::Texture& texture);
	~Bonus();
	/*Tenemos distintos tipos de frutas...
	cerezas,platanos,manzana
	por lo que tenemos que dejarlo claro en una enumerativo*/
	enum Fruit
	{
		Banana,
		Apple,
		Cherry
	};
	/*Para que se contruirlo bien necesitamos acceder al
	spriteshett correcto*/
private:

	sf::Sprite m_visual;
	/*Siempre cuando hacemos una herencia de cualquier clase
	drawable tenemos que sobrescribir el metodo draw*/
	void Draw(sf::RenderTarget& target, sf::RenderStates states) const;

	void SetFruit(Fruit fruit);
};
#endif // ! PACWOMAN_BONUS_HPP
#include "Dot.h"



Dot::Dot()
{
}


Dot::~Dot()
{
}

sf::CircleShape Dot::getDot()
{
	sf::CircleShape dot;
	dot.setRadius(4);
	dot.setOrigin(2, 2);
	return dot;
}

sf::CircleShape Dot::getSuperDot()
{
	sf::CircleShape superdot;
	superdot.setRadius(8);
	superdot.setOrigin(4, 4);
	return superdot;
}

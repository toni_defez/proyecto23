#include "Bonus.h"



Bonus::Bonus(sf::Texture& texture) : m_visual(texture)
{
	m_visual.setOrigin(15, 15);
	SetFruit(Banana);
}


Bonus::~Bonus()
{
}

void Bonus::Draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(m_visual, states);
}

/*En funcion de la fruta que nos pasan usaremos 
un sprite u otro*/
void Bonus::SetFruit(Fruit fruit)
{
	if (fruit == Banana)
		/*SetTextureRect nos permite darle una textura
		cuadrada y despues a traves de su x,y y su ancho y alto
		completamos al marca*/
		m_visual.setTextureRect(sf::IntRect(32, 0, 30, 30));
	else if (fruit == Apple)
		m_visual.setTextureRect(sf::IntRect(32 + 30, 0, 30, 30));
	else if (fruit == Cherry)
		m_visual.setTextureRect(sf::IntRect(32 + 60, 0, 30, 30));
}



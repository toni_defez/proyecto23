#ifndef PACWOMAN_GHOST_HPP
#define PACWOMAN_GHOST_HPP
#include "Character.h"
#include "PacWoman.h"
/*
	
*/
class Ghost :
	public Character
{
	/*El fantasma tiene dos estados
	1)Estado fuerte(strong): Si el fantasma toca al pacwoman
					 esta desaparece.
	2)Estado debil(weak): Si pacwoman toca al fantasma este
					desaparece
	*/
public:
	enum State
	{
		Strong,
		Weak
	};

	void setWeak(sf::Time duration);
	bool isWeak() const;

	Ghost(sf::Texture& texture);
	~Ghost();
private:
	void draw(sf::RenderTarget& target, sf::RenderStates states)const;
	sf::Sprite m_visual;
	/*
	Parametros para controlar el estado debil 
	del enemigo
	*/
	sf::Time m_weaknessDuration;
	bool m_isWeak;
};
#endif

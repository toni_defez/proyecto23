#include "GameState.h"
#include "Game.h"

/*************/
//Esta funcion sirve para poner cualquier elemento
//su punto de origen en el centro

template <typename T>
void centerOrigin(T& drawable)
{
	sf::FloatRect bound = drawable.getLocalBounds();
	drawable.setOrigin(bound.width / 2, bound.height / 2);
}
/***************/


GameState::GameState(Game* game)
	:m_game(game)
{
}

Game * GameState::getGame() const
{
	return m_game;
}


GameState::~GameState()
{
}

void GameState::insertCoin()
{
}

void GameState::pressButton()
{
}

void GameState::moveStick(sf::Vector2i direction)
{
}

void GameState::Update(sf::Time delta)
{
}

void GameState::Draw(sf::RenderWindow & window)
{
}


/************************************************/
/*Estados*/
NoCoinState::NoCoinState(Game* game):GameState(game)
{
	m_sprite.setTexture(game->getLogo());
	m_sprite.setPosition(20, 50);
	m_text.setFont(game->getFont());
	m_text.setString("Insert coin");
	centerOrigin(m_text);
	m_text.setPosition(240, 150);  
	display_text = true;
}
void NoCoinState::insertCoin()
{
	//pulsamos  i y nos vamos a la pantalla de 
	//de esta manera pasamos a otro estado
	//
	getGame()->ChangeGameState(GameState::GetReady);
}
void NoCoinState::pressButton()
{
	
}

void NoCoinState::moveStick(sf::Vector2i direction)
{
	std::cout << "Hola" << std::endl;
}

void NoCoinState::Update(sf::Time delta)
{
	/*En update las cosas van asi
	tenemos una variable de tiempo que indicara
	cuando tenemos que dibujar el texto*/
	static sf::Time timeBuffer = sf::Time::Zero;
	timeBuffer += delta;

	while (timeBuffer >= sf::seconds(0.5))
	{
		display_text = !display_text;
		timeBuffer -= sf::seconds(1);
	}
}

void NoCoinState::Draw(sf::RenderWindow& window)
{
	window.draw(m_sprite);
	/*Con esto conseguimos que el texto parpade
	mirar update*/
	if(display_text)
		window.draw(m_text);
}

/********************/
GetReadyState::GetReadyState(Game * game) :GameState(game)
{
	m_text.setFont(game->getFont());
	m_text.setString("Are you ready?");
	m_text.setCharacterSize(14);
	centerOrigin(m_text);
	m_text.setPosition(250, 205);
}

void GetReadyState::insertCoin()
{
}

void GetReadyState::pressButton()
{
	getGame()->ChangeGameState(GameState::Playing);
}

void GetReadyState::moveStick(sf::Vector2i direction)
{
}

void GetReadyState::Update(sf::Time delta)
{
}

void GetReadyState::Draw(sf::RenderWindow & window)
{
	window.draw(m_text);
}
/*******************************/
PlayingState::PlayingState(Game * game) :GameState(game)
{
	m_text.setFont(game->getFont());
	m_text.setString("Play the game!!");
	m_text.setCharacterSize(14);
	centerOrigin(m_text);
	m_text.setPosition(250, 205);
}

void PlayingState::insertCoin()
{
}

void PlayingState::pressButton()
{
}

void PlayingState::moveStick(sf::Vector2i direction)
{
	/*Pruebas para el cambio de navegacion
	lo haremos asi
	Cuando pulse derecha ire a win
	Cuando pulse izquierda ire a loose
	*/

	if (direction.x == -1)
		getGame()->ChangeGameState(GameState::Lost);
	else if (direction.x == 1)
		getGame()->ChangeGameState(GameState::Won);
}

void PlayingState::Update(sf::Time delta)
{
}

void PlayingState::Draw(sf::RenderWindow & window)
{
	window.draw(m_text);
}
/******************/
LostState::LostState(Game * game) :GameState(game)
{
	m_text.setFont(game->getFont());
	m_text.setString("Looser!!");
	m_text.setCharacterSize(42);
	centerOrigin(m_text);
	m_text.setPosition(240, 120);

	m_countDownText.setFont(game->getFont());
	m_countDownText.setCharacterSize(12);
	m_countDownText.setPosition(240, 240);

}

void LostState::insertCoin()
{
}

void LostState::pressButton()
{
}

void LostState::moveStick(sf::Vector2i direction)
{
}

void LostState::Update(sf::Time delta)
{
	/*
	Este update lo que hace es contar los 
	siguientes 10 segundos para cambiar a la pantalla 
	de inicio
	*/

	m_countDown += delta;
	if (m_countDown.asSeconds() >= 10)
		getGame()->ChangeGameState(GameState::NoCoin);

	m_countDownText.setString("Insert a coin to continue..."
		+ std::to_string(10 - static_cast<int>(m_countDown.asSeconds())));
 }

void LostState::Draw(sf::RenderWindow & window)
{
	window.draw(m_text);
	window.draw(m_countDownText);
}
/******************/
WonState::WonState(Game * game) :GameState(game)
{
	m_text.setFont(game->getFont());
	m_text.setString("Winner!!");
	m_text.setCharacterSize(42);
	centerOrigin(m_text);
	m_text.setPosition(240, 120);
}

void WonState::insertCoin()
{
}

void WonState::pressButton()
{
}

void WonState::moveStick(sf::Vector2i direction)
{
}

void WonState::Update(sf::Time delta)
{ 
	//Aqui incrementamos el time buffer hasta
	//que llegue a cinco en ese punto lo que haremos
	//sera cambiar de pantalla a la de ready

	static sf::Time timeBuffer = sf::Time::Zero;
	timeBuffer += delta;
	if (timeBuffer.asSeconds() > 5)
	{
		getGame()->ChangeGameState(GameState::GetReady);
	}
}

void WonState::Draw(sf::RenderWindow & window)
{
	window.draw(m_text);
}
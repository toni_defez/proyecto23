#ifndef PACWOMAN_PACWOMAN_HPP
#define PACWOMAN_PACWOMAN_HPP
#include "Character.h"

class PacWoman :
	public Character
{
public:
	PacWoman(sf::Texture& texture);
	~PacWoman();
	void Die();
	bool isDead() const;

private:
	void draw(sf::RenderTarget& target, sf::RenderStates states)const;
	sf::Sprite m_visual;
	bool m_isDead;
	sf::Sprite m_visual;


};
#endif


#ifndef PACWOMAN_GAMESTATE_HPP
#define PACWOMAN_GAMESTATE_HPP
/*Lo de las cabeceras se usan para mejorar
el timempo de compilacion 
Se ponen unicamente en los archivos de cabecera
*/
/*
El gameState engloba las clase generica de la clase
asi como los diferentes estados en que podemos encontrar
en el juego.
En un principio definir varias clases en un unico h.

*/

#include "SFML\Graphics.hpp"
#include <iostream>
class Game;

class GameState
{
public:
	/*El constructor necesita un juego
	padre*/
	GameState(Game* game);
	/*Dar el juego padre*/
	Game* getGame() const;
	~GameState();
	/*El tipo enum indica los diferentes
	estados que nos podremos encontrar 
	en el juego*/
	enum State
	{
		NoCoin,
		GetReady,
		Playing,
		Won,
		Lost,
		Count  //Con este valor conseguimos
			   //la cantidad de elementos que hay
			   //En el enum
	};

	//todo estado necesitara las funcionalidades
	//basicas de los estados de juego
	virtual void insertCoin();
	virtual void pressButton();
	virtual void moveStick(sf::Vector2i direction);
	virtual void Update(sf::Time delta);
	virtual void Draw(sf::RenderWindow& window);

	bool display_text;
	private:
		Game* m_game;
};
/*Cada uno de los estados tendra su implementacion 
de los metodos de la clase GameState*/
/************************************************/
//Maquina de estados finito
//Estado de no coin
class NoCoinState : public GameState
{
	public:
	NoCoinState(Game* game);

	void insertCoin();
	void pressButton();
	void moveStick(sf::Vector2i direction);
	void Update(sf::Time delta);
	void Draw(sf::RenderWindow& window);

	private:
		sf::Text m_text;
		sf::Sprite m_sprite;

};
/*****************/
//GetReady
class GetReadyState : public GameState
{
public:
	GetReadyState(Game* game);
	void insertCoin();
	void pressButton();
	void moveStick(sf::Vector2i direction);
	void Update(sf::Time delta);
	void Draw(sf::RenderWindow& window);
private:
	sf::Text m_text;
	
};

/***************************/

class PlayingState : public GameState
{
public:
	PlayingState(Game* game);
	void insertCoin();
	void pressButton();
	void moveStick(sf::Vector2i direction);
	void Update(sf::Time delta);
	void Draw(sf::RenderWindow& window);
private:
	sf::Text m_text;
};

/***************************/
//LostState
class LostState : public GameState
{
public:
	LostState(Game* game);
	void insertCoin();
	void pressButton();
	void moveStick(sf::Vector2i direction);
	void Update(sf::Time delta);
	void Draw(sf::RenderWindow& window);
private:
	sf::Text m_text;
	sf::Time m_countDown;
	sf::Text m_countDownText;
};

class WonState : public GameState
{
public:
	WonState(Game* game);
	void insertCoin();
	void pressButton();
	void moveStick(sf::Vector2i direction);
	void Update(sf::Time delta);
	void Draw(sf::RenderWindow& window);
private:
	sf::Text m_text;
};

#endif  //PACWOMAN_GAMESTATE_HPP






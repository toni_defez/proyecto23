#ifndef PACWOMAN_DOT_HPP
#define PACWOMAN_DOT_HPP
#include <SFML\Graphics.hpp>
class Dot
{
public:
	Dot();
	~Dot();
	/*Ya tenemos un propiedad en sfml
	que se puede ocupar del dibujo de
	los puntos*/
	sf::CircleShape getDot();
	sf::CircleShape getSuperDot();
};
#endif //PACWOMAN_DOT_HPP
